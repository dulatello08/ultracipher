package net.dulatello08.ultracipher

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "keys")
data class KeyObject(
    @PrimaryKey(autoGenerate = true) val id: Int=0,
    @ColumnInfo(name="key") val key: String
)
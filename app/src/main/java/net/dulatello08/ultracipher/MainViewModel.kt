package net.dulatello08.ultracipher

import android.util.Base64
import androidx.lifecycle.ViewModel

class MainViewModel: ViewModel() {
    private val charset = Charsets.UTF_8
    fun cipher(input: String, key: String): String {
        val rc4 = RC4(key.toByteArray(charset))
        return Base64.encode(rc4.encrypt(input.toByteArray(charset)), Base64.DEFAULT).toString(charset)
    }
    fun decipher(cipherText: String, key: String): String {
        val rc4 = RC4(key.toByteArray(charset))
        rc4.reset()
        //return Base64.decode(rc4.decrypt(cipherText.toByteArray(charset)), Base64.DEFAULT).toString(charset)
        return rc4.decrypt(Base64.decode(cipherText.toByteArray(charset), Base64.DEFAULT)).toString(charset)
    }
}
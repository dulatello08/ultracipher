package net.dulatello08.ultracipher

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            // User chose the "Settings" item, show the app settings UI...
            supportFragmentManager.commit {
                setCustomAnimations(R.anim.slide_in, R.anim.slide_out)
                replace(R.id.fragmentContainerView, SettingsFragment())
                addToBackStack(null)
            }
            true
        }

        R.id.action_key_history -> {
            // User chose the "Key history" action, show key history
            supportFragmentManager.commit {
                setCustomAnimations(R.anim.slide_in, R.anim.slide_out)
                replace(R.id.fragmentContainerView, KeyHistoryFragment())
                addToBackStack(null)
            }
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }
}
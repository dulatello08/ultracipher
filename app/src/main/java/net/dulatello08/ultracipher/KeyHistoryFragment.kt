package net.dulatello08.ultracipher

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class KeyHistoryFragment : Fragment() {

    companion object {
        fun newInstance() = KeyHistoryFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val db = Room.databaseBuilder(
            requireContext(),
            AppDB::class.java, "key-list.db"
        ).build()
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_key_history, container, false)
        val keyList: RecyclerView = view.findViewById(R.id.keyList)
        keyList.layoutManager = LinearLayoutManager(requireContext())
        val keys = lifecycleScope.async(Dispatchers.IO) { return@async db.keyDao().getAll()}
        lifecycleScope.launch{keyList.adapter = KeyHistoryAdapter(keys.await())}
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel by viewModels<KeyHistoryViewModel>()
        // TODO: Use the ViewModel
    }

}
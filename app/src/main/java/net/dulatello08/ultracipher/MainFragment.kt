package net.dulatello08.ultracipher

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.room.Room
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.make
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.dulatello08.ultracipher.databinding.FragmentMainBinding


class MainFragment: Fragment(R.layout.fragment_main) {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private var mInterstitialAd: InterstitialAd? = null
    private lateinit var mAdView : AdView
    private final var TAG = "ADS"
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        //cut here
        setHasOptionsMenu(true)
        val cipher=binding.cipher
        val decipher=binding.decipher
        val inputText=binding.inputText
        val key=binding.secretKey
        val result=binding.outputText
        MobileAds.initialize(requireActivity()) {}

        mAdView = binding.adView
        val adRequestBanner = AdRequest.Builder().build()
        mAdView.loadAd(adRequestBanner)

        val adRequest = AdRequest.Builder().build()
        fun load(){
            InterstitialAd.load(requireContext(),"ca-app-pub-7509088958653785/1557231852", adRequest, object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Log.d(TAG, adError.message)
                    mInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Log.d(TAG, "Ad was loaded.")
                    mInterstitialAd = interstitialAd
                }
            })
        }
        mInterstitialAd?.fullScreenContentCallback = object: FullScreenContentCallback() {
            override fun onAdDismissedFullScreenContent() {
                Log.d(TAG, "Ad was dismissed.")
            }

            override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                Log.d(TAG, "Ad failed to show.")
            }

            override fun onAdShowedFullScreenContent() {
                Log.d(TAG, "Ad showed fullscreen content.")
                mInterstitialAd = null
            }
        }
        //cut here
        val model: MainViewModel by viewModels()
        val db = Room.databaseBuilder(
            requireContext(),
            AppDB::class.java, "key-list.db"
        ).build()
        var counter = 0
        cipher.setOnClickListener {
            if(inputText.text.toString().isBlank() || key.text.toString().isBlank()){
                make(binding.root, "Please fill all required fields", Snackbar.LENGTH_SHORT).show()
            } else {
                counter++
                if (counter==5) {
                    mInterstitialAd?.show(requireActivity())
                    counter=0
                    load()
                }
                result.text = model.cipher(inputText.text.toString(), key.text.toString())
                if(PreferenceManager.getDefaultSharedPreferences(requireContext()).getBoolean("save_key", true))
                    lifecycleScope.launch(Dispatchers.IO){db.keyDao().insertAll(KeyObject(key=key.text.toString()))}
            }
        }
        decipher.setOnClickListener {
            if(inputText.text.toString().isBlank() || key.text.toString().isBlank()){
                make(binding.root, "Please fill all required fields", Snackbar.LENGTH_SHORT).show()
            } else {
                counter++
                if (counter==5) {
                    mInterstitialAd?.show(requireActivity())
                    counter=0
                    load()
                }
                val resultStr = model.decipher(inputText.text.toString(), key.text.toString())
                binding.outputText.text=resultStr
            }
        }
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.main_fragment, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}
package net.dulatello08.ultracipher

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface KeyObjectDAO {
    @Query("SELECT * FROM keys")
    fun getAll(): List<KeyObject>

    @Insert
    fun insertAll(vararg key: KeyObject)
}
package net.dulatello08.ultracipher

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [KeyObject::class], version = 1)
abstract class AppDB : RoomDatabase() {
    abstract fun keyDao(): KeyObjectDAO
}